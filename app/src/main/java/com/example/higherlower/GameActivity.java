package com.example.higherlower;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class GameActivity extends AppCompatActivity {

    private Game game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // setup the ui
        setContentView(R.layout.activity_game);

        this.game = new Game();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Game.Guess guess = v.getId() == R.id.lower
                        ? Game.Guess.LOWER
                        : Game.Guess.HIGHER;

                game.guess(guess);

                // decide if the game is over, and if it is, we need to pop back to the main landing page!
                if (game.isActive())
                    updateUi();
                else
                {
                    // game is over!
                    MainActivity.LAST_SCORE = game.getScore();

                    // pop this activity off the stack
                    finish();
                }
            }
        };

        this.findViewById(R.id.lower).setOnClickListener(listener);
        this.findViewById(R.id.higher).setOnClickListener(listener);

        // ensure that the layout of the UI matches the current game state!
        this.updateUi();
    }

    // Ensure that the user is seeing the state of the game correctly
    private void updateUi()
    {
        TextView cardInfoTextView = this.findViewById(R.id.cardInfo);
        cardInfoTextView.setText( this.game.getCard().toString() );

        TextView scoreTextView = this.findViewById(R.id.score);
        scoreTextView.setText("Your score is: " + this.game.getScore());
    }
}
