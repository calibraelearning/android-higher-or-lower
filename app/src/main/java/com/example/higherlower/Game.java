package com.example.higherlower;

public class Game
{
    private Deck deck;
    private int currentCardIndex;
    private boolean active;

    enum Guess {
        HIGHER,
        LOWER
    }

    public Game()
    {
        // prepare the deck
        this.deck = new Deck();
        this.deck.shuffle();
        this.currentCardIndex = 0;
        this.active = true;
    }

    public Card getCard()
    {
        return this.deck.cardAt(this.currentCardIndex);
    }

    private Card getNextCard()
    {
        return this.deck.cardAt(this.currentCardIndex + 1);
    }

    // Play a turn
    public void guess(Guess guess)
    {
        // prevent guesses being made on games that are over!
        if (!this.active)
            return;

        // get the next card
        Card nextCard = this.getNextCard();
        Card currentCard = this.getCard();

        boolean correct = this.isGuessCorrect(currentCard, nextCard, guess);
        if (correct)
        {
            this.currentCardIndex++;
            if (this.getCard() == null)
                this.active = false;
        }
        else
        {
            this.active = false;
        }
    }

    public boolean isGuessCorrect(Card currentCard, Card nextCard, Guess guess)
    {
        boolean notNull = currentCard != null && nextCard != null;

        return (notNull && nextCard.getValue() > currentCard.getValue() && guess.equals(Guess.HIGHER)) ||
                (notNull && nextCard.getValue() < currentCard.getValue() && guess.equals(Guess.LOWER));
    }

    public boolean isActive()
    {
        return active;
    }

    public int getScore()
    {
        return this.currentCardIndex * 100;
    }
}
