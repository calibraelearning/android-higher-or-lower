package com.example.higherlower;

public class Card
{
    enum Suit {
        SPADES,
        DIAMONDS,
        HEARTS,
        CLUBS
    }

    private Suit suit;

    // 1 - ace
    // 11 - jack
    // 12 - queen
    // 13 - king
    private int value;

    // constructor
    public Card(Card.Suit suit, int value)
    {
        this.suit = suit;
        this.value = value;
    }

    // returns the displayable version of the value
    public String getDisplayValue()
    {
        String result = this.value + "";

        switch(this.value)
        {
            case 1:
                result = "Ace";
                break;
            case 11:
                result = "Jack";
                break;
            case 12:
                result = "Queen";
                break;
            case 13:
                result = "King";
                break;
        }

        return result;
    }

    public String toString()
    {
        return this.getDisplayValue() + " " + this.suit;
    }

    public Suit getSuit()
    {
        return suit;
    }

    public int getValue()
    {
        return value;
    }
}