package com.example.higherlower;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // static / class variable to ensure we can upate the last score from anywhere in this app
    static Integer LAST_SCORE = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // wire up the click handler for the play button
        Button button = this.findViewById(R.id.play);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // pop the Game activity class
                Intent intent = new Intent(getApplication(), GameActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.updateLastScore();
    }

    private void updateLastScore()
    {
        if (MainActivity.LAST_SCORE != null)
        {
            TextView lastScore = this.findViewById(R.id.last);
            lastScore.setVisibility(View.VISIBLE);
            lastScore.setText("Last: " + MainActivity.LAST_SCORE);
        }
    }
}
