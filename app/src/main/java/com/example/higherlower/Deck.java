package com.example.higherlower;

import java.util.ArrayList;
import java.util.Collections;

public class Deck
{
    private ArrayList<Card> cards;

    public Deck()
    {
        this.cards = new ArrayList<>();
        for(Card.Suit suit: Card.Suit.values())
            for(int i=1; i<14; i++)
                this.cards.add(new Card(suit, i));
    }

    public void shuffle()
    {
        Collections.shuffle(this.cards);
    }

    public ArrayList<Card> getCards()
    {
        return this.cards;
    }

    public Card cardAt(int i)
    {
        // ensure that the index is within bounds
        return i < this.cards.size() && i >= 0
            ? this.cards.get(i)
            : null;
    }
}
