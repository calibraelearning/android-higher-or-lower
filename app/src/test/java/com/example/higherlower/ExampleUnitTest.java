package com.example.higherlower;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void card_isCorrect() {
        Card card = new Card(Card.Suit.DIAMONDS, 1);
        Assert.assertTrue(card.getDisplayValue().equals("Ace"));

        card = new Card(Card.Suit.DIAMONDS, 11);
        Assert.assertTrue(card.getDisplayValue().equals("Jack"));

        card = new Card(Card.Suit.DIAMONDS, 7);
        Assert.assertTrue(card.getDisplayValue().equals("7"));
    }

    @Test
    public void deck_isCorrect() {
        Deck deck = new Deck();
        Assert.assertNotNull(deck);
        Assert.assertEquals(deck.getCards().size(), 52);

        Assert.assertNull(deck.cardAt(52));
        Assert.assertNull(deck.cardAt(-100));
        Assert.assertNotNull(deck.cardAt(3));
    }

    @Test
    public void game_isCorrect() {
        Game game = new Game();
        Card lowerCard = new Card(Card.Suit.DIAMONDS, 4);
        Card higherCard = new Card(Card.Suit.CLUBS, 6);

        // regular conditions
        Assert.assertTrue( game.isGuessCorrect(lowerCard, higherCard, Game.Guess.HIGHER) );
        Assert.assertFalse( game.isGuessCorrect(higherCard, lowerCard, Game.Guess.HIGHER) );

        Assert.assertTrue( game.isGuessCorrect(higherCard, lowerCard, Game.Guess.LOWER) );
        Assert.assertFalse( game.isGuessCorrect(lowerCard, higherCard, Game.Guess.LOWER) );

        // What about the next card having the same value? Game over (Caleb rules)!
        Assert.assertFalse( game.isGuessCorrect(higherCard, higherCard, Game.Guess.LOWER) );
        Assert.assertFalse( game.isGuessCorrect(higherCard, higherCard, Game.Guess.HIGHER) );

        // See if we can ensure that the game will not explode on the last card
        Assert.assertFalse( game.isGuessCorrect(higherCard, null, Game.Guess.HIGHER) );
        Assert.assertFalse( game.isGuessCorrect(null, null, Game.Guess.HIGHER) );
        Assert.assertFalse( game.isGuessCorrect(null, higherCard, Game.Guess.HIGHER) );
    }
}